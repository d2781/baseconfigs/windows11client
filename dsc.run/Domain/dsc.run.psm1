Configuration DryDSCConfiguration {
    Param (
        [Parameter(Mandatory)] [String] $IPAddress
    )
    # Importing modules 
    Import-Module PSDesiredStateConfiguration

    <#
        Import-Module -Name 'xActiveDirectory' -RequiredVersion '3.0.0.0'
        Import-Module -Name 'xNetworking' -RequiredVersion '5.7.0.0'
        Import-Module -Name 'xDnsServer' -RequiredVersion '1.16.0.0'
    #>
    ###ImportModules###
    
    
    # Importing DSC Resources from modules
    Import-DSCResource -ModuleName PSDesiredStateConfiguration

    <#
    Import-DSCResource -Modulename 'xActiveDirectory' -Moduleversion '3.0.0.0'
    Import-DSCResource -Modulename 'xNetworking' -Moduleversion '5.7.0.0'
    Import-DSCResource -Modulename 'xDnsServer' -Moduleversion '1.16.0.0'
    #>
    ###ImportResources###
 
    Node $IPAddress
    {
        Registry NetworkLocationAwareness
        {
            Ensure      = "Present" 
            Key         = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\NlaSvc"
            ValueName   = "DelayedAutostart"
            ValueData   = "1"
            ValueType	= "Dword"
        }

        Registry DisableIPv6
        {
            Ensure      = "Present" 
            Key         = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip6\Parameters"
            ValueName   = "DisabledComponents"
            ValueData   = "255"
            ValueType	= "Dword"
        }

        Script SetNicInterfaceToRegisterInDns {
            GetScript = {
                $InterfaceExist = 'ExistsNot'
                [array]$RegistryInterfaces = @(Get-ChildItem -Path HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces)
                [array]$RegistryInterfacesPaths = $RegistryInterfaces.Name
                Foreach ($InterfacePath in $RegistryInterfacesPaths) {
                    $Interface = Get-ItemProperty -Path ($InterfacePath -Replace 'HKEY_LOCAL_MACHINE','HKLM:\')
                    If ($Interface.IPAddress -eq $using:IPAddress) {
                        $InterfaceExist = 'Exists'
                        $RegEnabled = $Interface.RegistrationEnabled 
                        $RegisterAdaptername = $Interface.RegisterAdapterName
                    }
                }
                Write-Verbose "Interface exists: '$InterfaceExist'"
                Write-Verbose "RegistrationEnabled: '$RegEnabled'"
                Write-Verbose "RegisterAdaptername: '$RegisterAdaptername'"
                @{
                }
            }

            TestScript = {
                $InterfaceExist = $False
                $ReturnTrue = $False
                [array]$RegistryInterfaces = @(Get-ChildItem -Path HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces)
                [array]$RegistryInterfacesPaths = $RegistryInterfaces.Name
                Foreach ($InterfacePath in $RegistryInterfacesPaths) {
                    $Interface = Get-ItemProperty -Path ($InterfacePath -Replace 'HKEY_LOCAL_MACHINE','HKLM:\')
                    If ($Interface.IPAddress -eq $using:IPAddress) {
                        Write-Verbose "Interface with correct IP found."
                        $InterfaceExist = $True
                        if (($Interface.RegistrationEnabled -eq 1) -and ($Interface.RegisterAdapterName -eq 1)) {
                            Write-Verbose "Correct values already set."
                            $ReturnTrue = $True 
                        } 
                    }
                }
                If (-not ($InterfaceExist)) { 
                    Write-Verbose "Interface with correct IP not found."    
                    $ReturnTrue = $True
                }
                If ($ReturnTrue) { 
                    Write-Verbose "Returning True - correct values already set."
                    Return $True
                } 
                Else { 
                    Write-Verbose "Returning False - values currently incorrect."
                    Return $False
                }
            }

            SetScript = {
                [array]$RegistryInterfaces = @(Get-ChildItem -Path HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces)
                [array]$RegistryInterfacesPaths = $RegistryInterfaces.Name
                Foreach ($InterfacePath in $RegistryInterfacesPaths) {
                    $Interface = Get-ItemProperty -Path ($InterfacePath -Replace 'HKEY_LOCAL_MACHINE','HKLM:\')
                    If ($Interface.IPAddress -eq $using:IPAddress) {
                        Write-Verbose "Setting regvalues RegistrationEnabled and RegisterAdaptername"
                        Set-ItemProperty -Path $($Interface.PSPath) -Name RegistrationEnabled -Value 1
                        Set-ItemProperty -Path $($Interface.PSPath) -Name RegisterAdapterName -Value 1
                    }
                }
            }
        }
    }
}